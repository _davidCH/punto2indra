﻿using Punto2.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Punto2.Core.Intefaces
{
    public interface IApp
    {
        ResponseDTO Calcular(RequestDTO data);
    }
}
