﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Punto2.Core.DTOs
{
    public class RequestDTO
    {
        public int[] lstPaquetes { get; set; }
        public int tamanioCamion { get; set; }
    }
}
