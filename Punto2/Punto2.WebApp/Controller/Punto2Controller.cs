﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Punto2.Core.DTOs;
using Punto2.Core.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Punto2.WebApp.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class Punto2Controller : ControllerBase
    {
        private IApp _App;
        public Punto2Controller(IApp app)
        {
            _App = app;
        }
        /// <summary>
        /// End point encargado de determinar que se debe cargar en el camion
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody] RequestDTO request)
        {
            ResponseDTO response = _App.Calcular(request);
            return Ok(response.result);
        }
    }
}
