﻿using Punto2.Core.DTOs;
using Punto2.Core.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Punto2.Application
{
    public class App : IApp
    {
        public ResponseDTO Calcular(RequestDTO data)
        {
            ResponseDTO result = new ResponseDTO();
            result.result = new int[2];
            bool finalizar = false;
            int Suma_maxima = data.tamanioCamion - 30;
            int[] dataAnalisis;

            dataAnalisis = data.lstPaquetes
                            .Where(d => d < Suma_maxima)
                            .OrderByDescending(d=>d)
                            .ToArray();
            for (var i=0;i<dataAnalisis.Length - 1; i++)
            {
                for (var j=1;j<dataAnalisis.Length;j++)
                {
                    if (dataAnalisis[i] + dataAnalisis[j] == Suma_maxima)
                    {
                        result.result[0] = dataAnalisis[i];
                        result.result[1] = dataAnalisis[j];
                        finalizar = true;
                        break;
                    }
                }
                if (finalizar)
                    break;
                
            }
            return result;
        }
    }
}
